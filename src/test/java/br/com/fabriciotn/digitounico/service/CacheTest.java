package br.com.fabriciotn.digitounico.service;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.fabriciotn.digitounico.cache.LRUCache;
import br.com.fabriciotn.digitounico.model.DigitoUnico;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CacheTest {

	private static final LRUCache<Long, DigitoUnico> cache = LRUCache.newInstance(3);
	
	@Test
	public void manterUltimosTresRegistrosNoCacheTest() {
		DigitoUnico digito1 = new DigitoUnico("1", 1, 0);
		DigitoUnico digito2 = new DigitoUnico("2", 2, 0);
		DigitoUnico digito3 = new DigitoUnico("3", 3, 0);
		DigitoUnico digito4 = new DigitoUnico("4", 4, 0);
		DigitoUnico digito5 = new DigitoUnico("5", 5, 0);
		
		cache.put((long) digito1.getQuantidadeConcatenacao(), digito1);
		cache.put((long) digito2.getQuantidadeConcatenacao(), digito2);
		cache.put((long) digito3.getQuantidadeConcatenacao(), digito3);
		cache.put((long) digito4.getQuantidadeConcatenacao(), digito4);
		cache.put((long) digito5.getQuantidadeConcatenacao(), digito5);

		assertFalse(cache.containsKey((long) digito1.getQuantidadeConcatenacao()));
		assertFalse(cache.containsKey((long) digito2.getQuantidadeConcatenacao()));
		assertTrue(cache.containsKey((long) digito3.getQuantidadeConcatenacao()));
		assertTrue(cache.containsKey((long) digito4.getQuantidadeConcatenacao()));
		assertTrue(cache.containsKey((long) digito5.getQuantidadeConcatenacao()));
	}
}
