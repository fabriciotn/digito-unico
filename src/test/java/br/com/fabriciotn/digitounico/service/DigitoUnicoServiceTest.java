package br.com.fabriciotn.digitounico.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class DigitoUnicoServiceTest {

	private static final String STRING_9875 = "9875";
	private static final int NUMERO_0 = 0;
	private static final int QUANTIDADE_ESPERADA_2 = 2;
	private static final int QUANTIDADE_ESPERADA_3 = 3;

	@Autowired
	private DigitoUnicoService digitoUnicoService;

	@Test
	public void calcularDigitoUnicoCorretoTest() {
		assertEquals(QUANTIDADE_ESPERADA_2, digitoUnicoService.digitoUnico(STRING_9875, NUMERO_0));
	}

	@Test
	public void falhaCalcularDigitoUnicoTest() {
		assertNotEquals(QUANTIDADE_ESPERADA_3, digitoUnicoService.digitoUnico(STRING_9875, NUMERO_0));
	}
}
