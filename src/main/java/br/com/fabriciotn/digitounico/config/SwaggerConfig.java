package br.com.fabriciotn.digitounico.config;

import static springfox.documentation.builders.PathSelectors.regex;

import java.util.ArrayList;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.service.VendorExtension;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfig {

	@Bean
	public Docket productApi() {
		return new Docket(DocumentationType.SWAGGER_2)
				.select()
				.apis(RequestHandlerSelectors.basePackage("br.com.fabriciotn.digitounico"))
				.paths(regex("/api.*"))
				.build()
				.apiInfo(metaInfo());
	}

	private ApiInfo metaInfo() {
		@SuppressWarnings("rawtypes")
		ApiInfo apiInfo = new ApiInfo(
				"Desafio Banco Inter", 
				"API REST Dígito único", 
				"1.0", 
				"",
				new Contact(
						"Fabricio Nascimento", 
						"https://github.com/fabriciotn", 
						"fabriciotn82@gmail.com"), 
				"Apache License Version 2.0",
				"https://www.apache.org/licenses/LICENSE-2.0", 
				new ArrayList<VendorExtension>());

		return apiInfo;
	}

}