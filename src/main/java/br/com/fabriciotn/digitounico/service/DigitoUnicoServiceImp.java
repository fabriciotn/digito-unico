package br.com.fabriciotn.digitounico.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.fabriciotn.digitounico.cache.LRUCache;
import br.com.fabriciotn.digitounico.model.DigitoUnico;
import br.com.fabriciotn.digitounico.model.Usuario;
import br.com.fabriciotn.digitounico.repository.DigitoUnicoRepository;
import br.com.fabriciotn.digitounico.repository.UsuarioRepository;

@Service
public class DigitoUnicoServiceImp implements DigitoUnicoService {

	private static final LRUCache<Long, DigitoUnico> cache = LRUCache.newInstance(10);

	@Autowired
	UsuarioRepository usuarioRepository;

	@Autowired
	DigitoUnicoRepository digitoUnicoRepository;

	@Override
	public int digitoUnico(String numero, int quantidade) {
		DigitoUnico digito = new DigitoUnico(numero, quantidade, 0);
		long numeroConcatenado = concatenarString(numero, quantidade);

		if (isEstaNoCache(digito)) {
			digito.setResultado(recuperarDoCache(numeroConcatenado));
		} else {
			digito.setResultado(calcularDigitoUnico(numeroConcatenado, 0));
			adicionarNoCache(numeroConcatenado, digito);
		}

		return digito.getResultado();

	}

	@Override
	public Integer calcularDigitoUnicoIncluindoAUsuario(String numero, int quantidadeVezes, Long idUsuario) {
		Optional<Usuario> usuarioOptional = usuarioRepository.findById(idUsuario);

		if (!usuarioOptional.isPresent()) {
			throw new RuntimeException("Usuário não existe. Favor utilizar um usuário existente.");
		}

		Usuario usuario = usuarioOptional.get();
		int digitoUnicoCalculado = digitoUnico(numero, quantidadeVezes);
		DigitoUnico digitoObj = new DigitoUnico(numero, quantidadeVezes, digitoUnicoCalculado);

		usuario.getDigitosUnicos().add(digitoObj);

		usuarioRepository.save(usuario);
		return digitoUnicoCalculado;
	}

	@Override
	public List<DigitoUnico> listarDigitosPorUsuario(long idUsuario) {
		return recuperarUsuario(idUsuario).getDigitosUnicos();
	}

	private Usuario recuperarUsuario(long idUsuario) {
		Optional<Usuario> usuarioOptional = usuarioRepository.findById(idUsuario);

		if (!usuarioOptional.isPresent()) {
			throw new RuntimeException("Usuário não existe. Favor utilizar um usuário existente.");
		}

		Usuario usuario = usuarioOptional.get();
		return usuario;
	}

	/**
	 * Realiza de fato o cálculo do dígito único
	 * 
	 * @param numero
	 * @param somaMomentanea
	 * @return
	 */
	private int calcularDigitoUnico(long numero, int somaMomentanea) {
		if (numero < 1) {
			if (somaMomentanea < 10) {
				return somaMomentanea;
			} else {
				numero = somaMomentanea;
				somaMomentanea = 0;
				calcularDigitoUnico(numero, 0);
			}
		}
		somaMomentanea += (numero % 10);
		return calcularDigitoUnico(numero / 10, somaMomentanea);
	}

	/**
	 * Concatena a String pela quantidade de vezes passadas no parâmetro quantidade
	 * 
	 * @param numero
	 * @param quantidade
	 * @return
	 */
	private long concatenarString(String numero, int quantidade) {
		StringBuilder retornoString = new StringBuilder();
		Long retornoLong = 0l;

		retornoString.append(numero);

		for (int i = 1; i < quantidade; i++) {
			retornoString.append(numero);
		}

		try {
			retornoLong = Long.parseLong(retornoString.toString());
		} catch (NumberFormatException e) {
			throw new RuntimeException("Valor digitado: " + numero + "\nValor após a concatenação: " + retornoString.toString()
					+ "\nO número digitado não pode ser uma string e o valor final "
					+ "após a conctatenação não pode ser maior do que 9223372036854775807");
		}

		return retornoLong;
	}

	/**
	 * Adiciona dígito no cache
	 * 
	 * @param key
	 * @param digito
	 */
	private void adicionarNoCache(long key, DigitoUnico digito) {
		cache.put(key, digito);

	}

	/**
	 * Recupera digito do cache
	 * 
	 * @param key
	 * @return
	 */
	private int recuperarDoCache(long key) {
		return cache.get(key).getResultado();
	}

	/**
	 * Verifica se o dígito já existe no cache
	 * 
	 * @param digito
	 * @return
	 */
	private boolean isEstaNoCache(DigitoUnico digito) {
		return cache.containsValue(digito);
	}
}
