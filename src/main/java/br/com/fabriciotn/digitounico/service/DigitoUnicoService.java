package br.com.fabriciotn.digitounico.service;

import java.util.List;

import br.com.fabriciotn.digitounico.model.DigitoUnico;

public interface DigitoUnicoService {

	/**
	 * Realiza o cálculo do dígito único a partir de de um long passado.
	 * 
	 * @param numero
	 * @param quantidade
	 * @return
	 */
	public int digitoUnico(String numero, int quantidade);

	/**
	 * Realiza o cálculo do dígito único e atribui a um usuário
	 * 
	 * @param numero
	 * @param quantidadeVezes
	 * @param idUsuario
	 * @return
	 */
	public Integer calcularDigitoUnicoIncluindoAUsuario(String numero, int quantidadeVezes, Long idUsuario);

	/**
	 * Recupera a lista de digitos calculados de um usuário específico
	 * 
	 * @param idUsuario
	 * @return
	 */
	public List<DigitoUnico> listarDigitosPorUsuario(long idUsuario);
}
