package br.com.fabriciotn.digitounico.service;

import br.com.fabriciotn.digitounico.model.Usuario;

public interface UsuarioService {

	/**
	 * Cria ou altera um usuário
	 * 
	 * @param usuario
	 * @return
	 */
	public Usuario salvar(Usuario usuario) throws Exception ;

}
