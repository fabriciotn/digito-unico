package br.com.fabriciotn.digitounico.service;

import java.security.KeyPair;
import java.security.NoSuchAlgorithmException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.fabriciotn.digitounico.model.DigitoUnico;
import br.com.fabriciotn.digitounico.model.Usuario;
import br.com.fabriciotn.digitounico.repository.UsuarioRepository;
import br.com.fabriciotn.digitounico.util.RSA;

@Service
public class UsuarioServiceImp implements UsuarioService {

	@Autowired
	UsuarioRepository usuarioRepository;

	@Autowired
	DigitoUnicoService digitoUnicoService;

	@Override
	public Usuario salvar(Usuario usuario) throws Exception {
		calcularDigitoUnicoParaUsuario(usuario);
		criptografaCamposUsuario(usuario);
		return usuarioRepository.save(usuario);
	}

	/**
	 * Realiza a criptografia dos campos Nome e Email
	 * 
	 * @param usuario
	 * @throws NoSuchAlgorithmException
	 * @throws Exception
	 */
	private void criptografaCamposUsuario(Usuario usuario) throws NoSuchAlgorithmException, Exception {
		KeyPair keyPair = RSA.generateKey();
		// PublicKey publicKey = RSA.getPublicKeyFromString(usuario.getChavePublica());
		usuario.setEmail(RSA.encrypt(usuario.getEmail(), keyPair.getPublic()));
		usuario.setNome(RSA.encrypt(usuario.getNome(), keyPair.getPublic()));
	}

	/**
	 * Calcula o dígito único para o usuário
	 * 
	 * @param usuario
	 */
	private void calcularDigitoUnicoParaUsuario(Usuario usuario) {
		if (usuario.getDigitosUnicos() != null) {
			for (DigitoUnico digito : usuario.getDigitosUnicos()) {
				digito.setResultado(digitoUnicoService.digitoUnico(digito.getNumero(), digito.getQuantidadeConcatenacao()));
			}
		}
	}

}
