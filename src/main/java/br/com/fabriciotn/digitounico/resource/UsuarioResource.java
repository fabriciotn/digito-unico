package br.com.fabriciotn.digitounico.resource;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.fabriciotn.digitounico.model.Usuario;
import br.com.fabriciotn.digitounico.repository.UsuarioRepository;
import br.com.fabriciotn.digitounico.service.UsuarioService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api")
@Api(value = "API REST Usuários")
public class UsuarioResource {

	@Autowired
	UsuarioRepository usuarioRepository;
	
	@Autowired
	UsuarioService usuarioService;

	@ApiOperation(value = "Retorna uma lista de usuários")
	@GetMapping("/usuario")
	public List<Usuario> listarUsuarios() {
		return usuarioRepository.findAll();
	}

	@ApiOperation(value = "Retorna um único usuário")
	@GetMapping("/usuario/{id}")
	public Optional<Usuario> consultarUsuario(@PathVariable(value = "id") long id) {
		return usuarioRepository.findById(id);
	}

	@ApiOperation(value = "Salva um usuário")
	@PostMapping("/usuario")
	@ResponseStatus(code=HttpStatus.CREATED)
	public Usuario salvarUsuario(@RequestBody @Valid Usuario usuario) throws Exception {
		return usuarioService.salvar(usuario);
	}

	@ApiOperation(value = "Deleta um usuário")
	@DeleteMapping("/usuario")
	public void deletarUsuario(@RequestBody @Valid Usuario usuario) {
		usuarioRepository.delete(usuario);
	}

	@ApiOperation(value = "Atualiza um usuário")
	@PutMapping("/usuario")
	public Usuario atualizarUsuario(@RequestBody @Valid Usuario usuario) throws Exception {
		return usuarioService.salvar(usuario);
	}
}
