package br.com.fabriciotn.digitounico.resource;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.fabriciotn.digitounico.model.DigitoUnico;
import br.com.fabriciotn.digitounico.service.DigitoUnicoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api")
@Api(value = "API REST Dígito Único")
public class DigitoUnicoResource {

	@Autowired
	private DigitoUnicoService digitoUnicoService;

	@ApiOperation(value = "Realiza somente o cálculo do dígito único.")
	@PostMapping("/digito-unico/{numero}/{quantidadeVezes}")
	public Integer calcularDigitoUnico(@PathVariable(value = "numero") String numero, @PathVariable(value = "quantidadeVezes") int quantidadeVezes) {
		return digitoUnicoService.digitoUnico(numero, quantidadeVezes);
	}

	@ApiOperation(value = "Realiza o cálculo do dígito único e atribui o cálculo a um usuário já existente.")
	@PostMapping("/digito-unico/{numero}/{quantidadeVezes}/{idUsuario}")
	public Integer calcularDigitoUnicoParaUsuario(@PathVariable(value = "numero") String numero,
			@PathVariable(value = "quantidadeVezes") int quantidadeVezes, @PathVariable(value = "idUsuario") Long idUsuario) {
		return digitoUnicoService.calcularDigitoUnicoIncluindoAUsuario(numero, quantidadeVezes, idUsuario);
	}

	@ApiOperation(value = "Retorna a lista de cálculos de um usuário")
	@GetMapping("/digito-unico/{idUsuario}")
	public List<DigitoUnico> consultarUsuario(@PathVariable(value = "idUsuario") long idUsuario) {
		return digitoUnicoService.listarDigitosPorUsuario(idUsuario);
	}

}
