package br.com.fabriciotn.digitounico.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

@Entity
public class DigitoUnico implements Serializable {

	private static final long serialVersionUID = 5566925139709608406L;

	public DigitoUnico() {
	}

	public DigitoUnico(@NotNull String numero, @NotNull int quantidadeConcatenacao, int resultado) {
		this.numero = numero;
		this.quantidadeConcatenacao = quantidadeConcatenacao;
		this.resultado = resultado;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NotNull
	private String numero;

	@NotNull
	private int quantidadeConcatenacao;

	private int resultado;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public int getQuantidadeConcatenacao() {
		return quantidadeConcatenacao;
	}

	public void setQuantidadeConcatenacao(int quantidadeConcatenacao) {
		this.quantidadeConcatenacao = quantidadeConcatenacao;
	}

	public int getResultado() {
		return resultado;
	}

	public void setResultado(int resultado) {
		this.resultado = resultado;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((numero == null) ? 0 : numero.hashCode());
		result = prime * result + quantidadeConcatenacao;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DigitoUnico other = (DigitoUnico) obj;
		if (numero == null) {
			if (other.numero != null)
				return false;
		} else if (!numero.equals(other.numero))
			return false;
		if (quantidadeConcatenacao != other.quantidadeConcatenacao)
			return false;
		return true;
	}
}
