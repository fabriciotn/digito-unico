package br.com.fabriciotn.digitounico.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.fabriciotn.digitounico.model.DigitoUnico;

public interface DigitoUnicoRepository extends JpaRepository<DigitoUnico, Long> {

}
