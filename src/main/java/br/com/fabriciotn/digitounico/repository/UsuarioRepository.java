package br.com.fabriciotn.digitounico.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.fabriciotn.digitounico.model.Usuario;

public interface UsuarioRepository extends JpaRepository<Usuario, Long> {

}
